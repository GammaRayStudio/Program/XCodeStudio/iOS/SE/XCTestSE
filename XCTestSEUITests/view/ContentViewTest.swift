//
//  ViewTest.swift
//  XCTestSEUITests
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import XCTest

class ContentViewTest: XCTestCase {
    
    override func setUp() {
        continueAfterFailure = false
    }
    
    func testInitialViewState() {
        let app = XCUIApplication()
        app.launch()
        
        let textField      = app.textFields.element
        let txtAppName = app.staticTexts["txtAppName"]
        let txtEnterAppName   = app.staticTexts["txtEnterAppName"]
        
        XCTAssert(txtAppName.exists)
        XCTAssertEqual(txtAppName.label, "App Name : XCTestSE")
        
        XCTAssert(txtEnterAppName.exists)
        XCTAssert(txtEnterAppName.label.isEmpty)
        
        XCTAssert(textField.exists)
        XCTAssertEqual(textField.placeholderValue, "app name")
    }
    
    func testEnterText() {
        let app = XCUIApplication()
        app.launch()
        
        let textLabel = app.staticTexts["txtEnterAppName"]
        let textField = app.textFields.element
        
        textField.tap()
        textField.typeText("X")
        textField.typeText("C")
        textField.typeText("T")
        textField.typeText("e")
        textField.typeText("s")
        textField.typeText("t")
        textField.typeText("S")
        textField.typeText("E")
        
        XCTAssertEqual(textLabel.label, "App Name -> XCTestSE.")
    }
    
}
