//
//  ContentView.swift
//  XCTestSE
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var name: String =  ""
    
    private var appName: String {
        name.isEmpty ? "" : "App Name -> \(name)."
    }
    
    var body: some View {
        VStack {
            Text("App Name : XCTestSE")
                .accessibility(identifier: "txtAppName")
            TextField("app name", text: $name)
            Text(appName)
                .accessibility(identifier: "txtEnterAppName")
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
