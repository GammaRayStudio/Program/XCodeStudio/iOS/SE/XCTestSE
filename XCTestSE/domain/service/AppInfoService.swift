//
//  AppInfoService.swift
//  XCTestSE
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import Foundation
class AppInfoService{
    
    var appInfo = AppInfo(name: "iOSProjSE" , version: "v1.0.1" , date: "2020-10-09" , author: "Enoxs" , remark: "iOS App : Tutor")
    
    func getAppInfo() -> AppInfo{
        return appInfo
    }
}
