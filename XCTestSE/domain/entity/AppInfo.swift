//
//  AppInfo.swift
//  XCTestSE
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import Foundation
class AppInfo{
    var name : String
    var version : String
    var date : String
    var author : String
    var remark : String
    
    init (name : String , version : String , date : String , author : String , remark : String){
        self.name = name
        self.version = version
        self.date = date
        self.author = author
        self.remark = remark
    }
    convenience init(){
        self.init(name: "" , version: "" , date: "" , author: "" , remark: "")
    }
}
