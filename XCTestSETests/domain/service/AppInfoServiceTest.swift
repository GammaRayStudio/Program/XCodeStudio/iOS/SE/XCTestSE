//
//  AppInfoServiceTest.swift
//  XCTestSETests
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import Foundation
import XCTest
@testable import XCTestSE

class AppInfoServiceTest : XCTestCase{
    var service = AppInfoService()
    func testGetAppInfo01(){
        let appInfo = service.getAppInfo()
        XCTAssertEqual("iOSProjSE", appInfo.name)
    }
    func testGetAppInfo02(){
        let appInfo = service.getAppInfo()
        XCTAssertEqual("v1.0.1", appInfo.version)
    }
}
