//
//  AsyncTest.swift
//  XCTestSETests
//
//  Created by Enoxs on 2020/10/9.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import XCTest

class AsyncTest: XCTestCase {

    var urlSession: URLSession!
    
    override func setUp() {
        urlSession = URLSession(configuration: .default)
    }

    override func tearDown() {
        urlSession = nil
    }
    
    /// 非同步的 API 回傳的狀態碼是否正確的測試
    func testAsynchronousAPI() {
        // Given
        var statusCode: Int?
        var responseError: Error?
        let url = URL(string: "https://www.google.com")
        let promise = expectation(description: "Invalid status code.") // 若是執行錯誤則會 log 出來的訊息
        
        // When
        let dataTask = urlSession.dataTask(with: url!) { data, response, error in
          statusCode = (response as? HTTPURLResponse)?.statusCode
          responseError = error
          promise.fulfill() // 表示非同步的任務已經結了，並告知 Wait() 可以結束了。表示可以繼續往下執行待驗證的數值。
        }
        
        dataTask.resume()
        wait(for: [promise], timeout: 8) // 設定被測試的 API 的 Timeout 時間
                
        // Then
        XCTAssertNil(responseError, "Response is error.")
        XCTAssertEqual(statusCode, 200, "Invalid status code.")
    }

}
